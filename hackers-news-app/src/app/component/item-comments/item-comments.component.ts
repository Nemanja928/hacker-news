import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item-comments',
  templateUrl: './item-comments.component.html',
  styleUrls: ['./item-comments.component.css']
})
export class ItemCommentsComponent implements OnInit {
  id: any;
  item;

  constructor(
    private apiService: ApiServiceService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {    
    this.id = this.route.params.subscribe(params => {
      let itemID = +params['id'];
      this.apiService.getComments(itemID).subscribe(response => {
        this.item = response;
        console.log(response.kids)
      });
    });
  }
}