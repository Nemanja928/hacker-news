import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.css']
})
export class StoriesComponent implements OnInit {
items;
showSpinner = true;
  constructor(private apiService: ApiServiceService) { }

  ngOnInit() { this.apiService.getStories()
    .subscribe(response => {
      this.items = response;
      console.log(response)
      this.showSpinner = false;
    });
}
}