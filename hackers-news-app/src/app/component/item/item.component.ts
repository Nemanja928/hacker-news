import { Component, Input, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {  
  @Input() itemID: number;
  item;

  constructor(private apiService: ApiServiceService) {}

  ngOnInit() {
    this.apiService.getItems(this.itemID).subscribe(response => {
      this.item = response;
      console.log(response)
    })
  }
}
