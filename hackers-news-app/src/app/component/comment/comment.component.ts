import { Component, OnInit, Input } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment;
  item;
  showSpinner = true;
  constructor(private apiService: ApiServiceService) { }

  ngOnInit() {
    this.apiService.getItems(this.comment).subscribe(response => {
      this.item = response;
      console.log(response);
      this.showSpinner = false;
    })
  }
}
