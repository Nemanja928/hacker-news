import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoriesComponent } from './component/stories/stories.component';
import { ItemCommentsComponent } from './component/item-comments/item-comments.component';


const routes: Routes = [
{path: '', redirectTo: 'item', pathMatch : 'full'},
{path: 'item', component: StoriesComponent},
{path: 'item/:id', component: ItemCommentsComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
