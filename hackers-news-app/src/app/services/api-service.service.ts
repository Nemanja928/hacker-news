import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'https://hacker-news.firebaseio.com/v0';
  }

  getStories(): Observable<any> {
    return this.http.get(`${this.baseUrl}/topstories.json`);
  }

  getItems(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/item/${id}.json`)
  }
  getComments(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/item/${id}.json`)
                    
  }
}